import re
import time
import praw
import OAuth2Util

# User config
# --------------------------------------------------------------------
# Don't include the /r/
SUBREDDIT_NAME = ''

# --------------------------------------------------------------------


def remove_post(subm, reason):
    if reason == 'np':
        comment = 'The post was not linked in no participation mode, so it has been removed.'
    elif reason == 'not_locked':
        comment = 'The linked thread is not locked, so it has been removed.'

    print('Removing post...')
    subm.add_comment(comment)
    subm.remove()


def check_if_thread_is_locked(r, o):
    submission_stream = praw.helpers.submission_stream(r, SUBREDDIT_NAME)
    print('Searching for posts...')
    np_regex = re.compile(r'http(s)?://np\.reddit[\s\S]')
    id_regex = re.compile(r'(?<=comments/)([A-z0-9-_])\w+')
    for submission in submission_stream:
        o.refresh()
        if submission.approved_by:
            # if the post has already been approved skip it
            continue
        if not np_regex.search(submission.url):
            # the post isn't linked in no partcipation remove and continue
            remove_post(submission, 'np')
            continue
        else:
            # have to get the id from the url, because praw can't get
            # submissions from np urls
            subm_id = id_regex.search(submission.url).group(0)
            linked = r.get_submission(submission_id=subm_id)
            if not linked.locked:
                remove_post(submission, 'not_locked')
            else:
                # approve the post as it passes all requirements
                print('Approving post...')
                submission.approve()


def main():
    r = praw.Reddit(user_agent='Approve_Locked v1.0 /u/cutety')
    o = OAuth2Util.OAuth2Util(r, print_log=True)

    while True:
        try:
            check_if_thread_is_locked(r, o)
        except Exception as e:
            print('ERROR: {}'.format(e))

        time.sleep(60)


if __name__ == '__main__':
    main()